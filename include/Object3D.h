#ifndef __OBJECT3D_H__
#define __OBJECT3D_H__

#include "Color.h"
#include "Coord3D.h"
#include "Ray.h"

class Object3D {

	public:
		Object3D(const Color &color);
		virtual ~Object3D();
		Color getColor() const;
		virtual bool intersects(const Ray &ray, Coord3D *intersectPoint = 0) const = 0;

	private:
		Color color;

};

#endif