#include "../include/Coord3D.h"

Coord3D::Coord3D(double x, double y, double z) : x(x), y(y), z(z) {
}

double Coord3D::getX() const {
	return x;
}

double Coord3D::getY() const {
	return y;
}

double Coord3D::getZ() const {
	return z;
}

Coord3D operator+(const Coord3D &a, const Coord3D &b) {
	return Coord3D(a.getX() + b.getX(), a.getY() + b.getY(), a.getZ() + b.getZ());
}

Coord3D operator-(const Coord3D &a, const Coord3D &b) {
	return Coord3D(a.getX() - b.getX(), a.getY() - b.getY(), a.getZ() - b.getZ());
}