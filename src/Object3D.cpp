#include "../include/Object3D.h"

Object3D::Object3D(const Color &color) :
	color(color) {
}

Object3D::~Object3D() {
}

Color Object3D::getColor() const {
	return color;
}