#include "../include/Vector3D.h"

#include <cmath>

Vector3D::Vector3D(double x, double y, double z) : Coord3D(x, y, z) {
}

Vector3D::Vector3D(const Coord3D &a, const Coord3D &b) :
	Vector3D(b.getX() - a.getX(), b.getY() - a.getY(), b.getZ() - a.getZ()) {
}

double Vector3D::scalarProduct(const Vector3D &other) const {
	return getX() * other.getX() + getY() * other.getY() + getZ() * other.getZ();
}

double Vector3D::getNorm2() const {
	return scalarProduct(*this); 
}

Vector3D Vector3D::normalized() const {
	const double norm = sqrt(getNorm2());
	return Vector3D(getX() / norm, getY() / norm, getZ() / norm);
}

Vector3D operator+(const Vector3D &a, const Vector3D &b) {
	return Vector3D(a.getX() + b.getX(), a.getY() + b.getY(), a.getZ() + b.getZ());
}

Vector3D operator-(const Vector3D &a, const Vector3D &b) {
	return Vector3D(a.getX() - b.getX(), a.getY() - b.getY(), a.getZ() - b.getZ());
}

Vector3D operator*(double a, const Vector3D &b) {
	return Vector3D(a * b.getX(), a * b.getY(), a * b.getZ());
}

Vector3D operator*(const Vector3D &a, double b) {
	return Vector3D(b * a.getX(), b * a.getY(), b * a.getZ());
}

Vector3D operator/(const Vector3D &a, double b) {
	return Vector3D(a.getX() / b, a.getY() / b, a.getZ() / b);
}