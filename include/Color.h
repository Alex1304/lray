#ifndef __COLOR_H__
#define __COLOR_H__

class Color {

	public:
		Color();
		Color(unsigned char red, unsigned char green, unsigned char blue);
		unsigned char getRed() const;
		unsigned char getGreen() const;
		unsigned char getBlue() const;

	private:
		unsigned char red;
		unsigned char green;
		unsigned char blue;

};

#endif