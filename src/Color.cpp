#include "../include/Color.h"

Color::Color() :
	red(0),
	green(0),
	blue(0) {
}

Color::Color(unsigned char red, unsigned char green, unsigned char blue) :
	red(red),
	green(green),
	blue(blue) {
}

unsigned char Color::getRed() const {
	return red;
}

unsigned char Color::getGreen() const {
	return green;
}

unsigned char Color::getBlue() const {
	return blue;
}