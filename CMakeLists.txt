cmake_minimum_required(VERSION 3.0)

project(raytracer)

set(CMAKE_CXX_FLAGS "-Wall -Werror -Wfatal-errors -g -O2 -std=c++11")

set(EXECUTABLE_OUTPUT_PATH ${PROJECT_BINARY_DIR}/bin)

file(MAKE_DIRECTORY ${PROJECT_BINARY_DIR}/output)

set(SRC_DIR src)
set(INCLUDE_DIR include)

set(SRCS
	${SRC_DIR}/Color.cpp
	${SRC_DIR}/Coord3D.cpp
	${SRC_DIR}/Image.cpp
	${SRC_DIR}/main.cpp
	${SRC_DIR}/Object3D.cpp
	${SRC_DIR}/Ray.cpp
	${SRC_DIR}/Scene.cpp
	${SRC_DIR}/Sphere.cpp
	${SRC_DIR}/Vector3D.cpp
)

set(INCLUDES
	${INCLUDE_DIR}/Color.h
	${INCLUDE_DIR}/Coord3D.h
	${INCLUDE_DIR}/Image.h
	${INCLUDE_DIR}/Object3D.h
	${INCLUDE_DIR}/Ray.h
	${INCLUDE_DIR}/Scene.h
	${INCLUDE_DIR}/Sphere.h
	${INCLUDE_DIR}/Vector3D.h
)

add_executable(lray ${SRCS} ${INCLUDES})