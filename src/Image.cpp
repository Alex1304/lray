#include "../include/Image.h"

#include <fstream>
#include <stdexcept>

using namespace std;

Image::Image(int width, int height) :
	width(width),
	height(height),
	pixels(width * height) {
}

int Image::getWidth() const {
	return width;
}

int Image::getHeight() const {
	return height;
}

Color Image::pixelAt(int x, int y) const {
	if (x < 0 || x >= width || y < 0 || y >= height) {
		throw out_of_range("pixel outside of image");
	}
	return pixels[width * y + x];
}

void Image::setPixel(int x, int y, const Color &value) {
	if (x < 0 || x >= width || y < 0 || y >= height) {
		throw out_of_range("pixel outside of image");
	}
	pixels[width * y + x] = value;
}

void Image::saveAs(const string &filename) const {
	ofstream imageFile(filename.c_str());
	if (!imageFile) {
		throw runtime_error("Unable to save image file");
	}
	imageFile << "P3" << endl;
	imageFile << to_string(width) << " " << to_string(height) << endl;
	imageFile << "255" << endl;
	for (long unsigned int i = 0 ; i < pixels.size() ; i++) {
		imageFile << to_string(pixels[i].getRed()) << " " << to_string(pixels[i].getGreen()) << " " << to_string(pixels[i].getBlue()) << endl;
	}
}

const Color &Image::operator[](int i) const {
	return pixels[i];
}

Color &Image::operator[](int i) {
	return pixels[i];
}