#include "../include/Scene.h"

#include <cmath>
#include <fstream>
#include <set>
#include <stdexcept>

#include "../include/Sphere.h"

#define PI 3.1415926535897932

using namespace std;

Scene::~Scene() {
	for (unsigned long int i = 0 ; i < objects.size() ; i++) {
		delete objects[i];
	}
}

typedef struct Intersection {
	double distance;
	Object3D *object;
	bool operator() (const Intersection &a, const Intersection &b) const {
		return a.distance < b.distance;
	}
} Intersection;

void Scene::castRay(Image &image, int x, int y) const {
	const Vector3D rayDirection(x - image.getWidth() / 2, y - image.getHeight() / 2, -image.getWidth() / (2 * tan(fieldOfView / 2)));
	const Ray ray(cameraPosition, rayDirection);
	set<Intersection, Intersection> intersections;
	for (unsigned long int i = 0 ; i < objects.size() ; i++) {
		Coord3D intersectPoint(0, 0, 0);
		if (objects[i]->intersects(ray, &intersectPoint)) {
			const Vector3D vectDistance(ray.getOrigin(), intersectPoint);
			Intersection inter;
			inter.distance = vectDistance.getNorm2();
			inter.object = objects[i];
			intersections.insert(inter);
		}
	}
	const Color color = intersections.empty() ? Color(0, 0, 0) : intersections.begin()->object->getColor();
	image.setPixel(x, y, color);
}

Scene *Scene::load(const string &filename) {
	ifstream sceneFile(filename.c_str());
	if (!sceneFile) {
		throw runtime_error("Error when loading scene file");
	}
	// Read camera position coordinates
	int cameraX, cameraY, cameraZ;
	sceneFile >> cameraX;
	sceneFile >> cameraY;
	sceneFile >> cameraZ;
	// Read field of view
	double fieldOfView;
	sceneFile >> fieldOfView;
	fieldOfView = fieldOfView * PI / 180;
	// Read objects
	vector<Object3D*> objects;
	while (!sceneFile.eof()) {
		string objectName;
		unsigned int r, g, b;
		sceneFile >> objectName;
		sceneFile >> r;
		sceneFile >> g;
		sceneFile >> b;
		if (!objectName.compare("sphere")) {
			int x, y, z;
			double radius;
			sceneFile >> x;
			sceneFile >> y;
			sceneFile >> z;
			sceneFile >> radius;
			objects.push_back(new Sphere(Color((unsigned char) r, (unsigned char) g, (unsigned char) b), Coord3D(x, y, z), radius));
		} else {
			break;
		}
	}
	return new Scene(objects, Coord3D(cameraX, cameraY, cameraZ), fieldOfView);
}

Scene::Scene(vector<Object3D*> &objects, const Coord3D &cameraPosition, double fieldOfView) :
	objects(objects),
	cameraPosition(cameraPosition),
	fieldOfView(fieldOfView) {
}