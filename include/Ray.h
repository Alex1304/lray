#ifndef __RAY_H__
#define __RAY_H__

#include "Coord3D.h"
#include "Vector3D.h"

class Ray {

	public:
		Ray(const Coord3D &origin, const Vector3D &direction);
		Coord3D getOrigin() const;
		Vector3D getDirection() const;

	private:
		Coord3D origin;
		Vector3D direction;

};

#endif