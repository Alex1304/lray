#include <iostream>

#include "../include/Color.h"
#include "../include/Coord3D.h"
#include "../include/Image.h"
#include "../include/Ray.h"
#include "../include/Scene.h"
#include "../include/Sphere.h"
#include "../include/Vector3D.h"

using namespace std;

int main(int argc, char **argv) {
	Image im(1024, 1024);
	const Scene *scene = Scene::load("input/scene.txt");
	cout << "Synthesizing the image..." << endl;
	for (int y = 0 ; y < im.getHeight() ; y++) {
		for (int x = 0 ; x < im.getWidth() ; x++) {
			scene->castRay(im, x, y);
		}
	}
	cout << "Synthesis done! Saving image..." << endl;
	im.saveAs("output/image.ppm");
	cout << "Image saved as output/image.ppm" << endl;
	delete scene;
	return 0;
}
