#ifndef __SCENE_H__
#define __SCENE_H__

#include <string>
#include <vector>

#include "Image.h"
#include "Object3D.h"
#include "Vector3D.h"

class Scene {

	public:
		~Scene();
		void castRay(Image &image, int x, int y) const;

		static Scene *load(const std::string &filename);

	private:
		std::vector<Object3D*> objects;
		Coord3D cameraPosition;
		double fieldOfView;

		Scene(std::vector<Object3D*> &objects, const Coord3D &cameraPosition, double fieldOfView);
};

#endif