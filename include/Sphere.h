#ifndef __SPHERE_H__
#define __SPHERE_H__

#include "Coord3D.h"
#include "Object3D.h"

class Sphere : public Object3D {

	public:
		Sphere(const Color &color, const Coord3D &center, double radius);
		virtual bool intersects(const Ray &ray, Coord3D *intersectPoint = 0) const;
		Coord3D getCenter() const;
		double getRadius() const;

	private:
		Coord3D center;
		double radius;
};

#endif