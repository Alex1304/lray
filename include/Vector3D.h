#ifndef __VECTOR3D_H__
#define __VECTOR3D_H__

#include "Coord3D.h"

class Vector3D : public Coord3D {

	public:
		Vector3D(double x, double y, double z);
		Vector3D(const Coord3D &a, const Coord3D &b);
		double scalarProduct(const Vector3D &other) const;
		double getNorm2() const;
		Vector3D normalized() const;
};

Vector3D operator+(const Vector3D &a, const Vector3D &b);
Vector3D operator-(const Vector3D &a, const Vector3D &b);
Vector3D operator*(double a, const Vector3D &b);
Vector3D operator*(const Vector3D &a, double b);
Vector3D operator/(const Vector3D &a, double b);

#endif