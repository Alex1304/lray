# LRay

A ray-tracing implementation in C++

## Features

* Generate a scene using a scene file
* The scene is made out of spheres which you choose the coordinates, the size and the color
* The synthesized image is saved as a .ppm file

## Missing features and known bugs

* It is not possible to choose the destination image file. It will always be saved at `output/image.ppm` from the working directory
* It is not possible to choose the scene file either, it will always be read at `input/scene.txt` from the working directory
* It does not support light sources and does not render shading effects. It only calculates intersections with the objects and applies the color directly without any transformation
* You can only put spheres in the scene. Other shapes are not implemented
* It is not possible to change camera orientation, only camera position.

## Installation

### Pre-requisites

* `cmake` version 3.0 or above

### Compiling

1. Create a new directory called `build` at the root of the project, and navigate to it

```sh
mkdir build
cd build
```

2. Generate the makefile with the following command

```sh
cmake
```

3. Compile the project

```sh
make
```

The executable is located at `bin/lray`

## Running the project

1. In the working directory, create a directory named `input` and create a file named `scene.txt`. This file has the following structure:

```
cameraX cameraY cameraZ fieldOfViewDegrees
objectType objectColor objectCoordinates
objectType objectColor objectCoordinates
objectType objectColor objectCoordinates
...
```

`objectType` must be equal to `sphere` as this is the only shape supported. `objectColor` is RGB value, each component separated with a space.
In the case of a sphere, `objectCoordinates` correspond to `centerX centerY centerZ radius`. Each new object is defined in a new line.

Example scene file:

```
0 0 20 60
sphere   255   0   0     0   0 -55    10
sphere     0 255   0    20 -10 -85    10
sphere     0   0 255   -10  10 -40    10
```

2. Execute the program

```sh
bin/lray
```

The image is saved in `output/image.ppm`