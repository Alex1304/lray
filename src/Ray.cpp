#include "../include/Ray.h"

Ray::Ray(const Coord3D &origin, const Vector3D &direction) :
	origin(origin),
	direction(direction.normalized()) {
}

Coord3D Ray::getOrigin() const {
	return origin;
}

Vector3D Ray::getDirection() const {
	return direction;
}