#ifndef __IMAGE_H__
#define __IMAGE_H__

#include <vector>
#include <string>

#include "Color.h"

class Image {

	public:
		Image(int width, int height);
		int getWidth() const;
		int getHeight() const;
		Color pixelAt(int x, int y) const;
		void setPixel(int x, int y, const Color &value);
		void saveAs(const std::string &filename) const;

		const Color &operator[](int i) const;
		Color &operator[](int i);

	private:
		int width;
		int height;
		std::vector<Color> pixels;

};

#endif