#include "../include/Sphere.h"

#include <cmath>

#include "../include/Vector3D.h"

Sphere::Sphere(const Color &color, const Coord3D &center, double radius) :
	Object3D(color),
	center(center),
	radius(radius) {
}

bool Sphere::intersects(const Ray &ray, Coord3D *intersectPoint) const {
	const Vector3D l(ray.getOrigin(), center);
	const double d = l.scalarProduct(ray.getDirection());
	const double l2 = l.scalarProduct(l);
	const double r2 = radius * radius;
	if (d < 0. && l2 > r2) {
		return false;
	}
	const double m2 = l2 - d * d;
	if (m2 > r2) {
		return false;
	}
	const double q = sqrt(r2 - m2);
	const double t = l2 > r2 ? d - q : d + q;
	if (intersectPoint != 0) {
		*intersectPoint = ray.getOrigin() + t * ray.getDirection();
	}
	return true;
}

Coord3D Sphere::getCenter() const {
	return center;
}

double Sphere::getRadius() const {
	return radius;
}
