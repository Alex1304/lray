#ifndef __COORD3D_H__
#define __COORD3D_H__

class Coord3D {

	public:
		Coord3D(double x, double y, double z);
		double getX() const;
		double getY() const;
		double getZ() const;

	private:
		double x;
		double y;
		double z;

};

Coord3D operator+(const Coord3D &a, const Coord3D &b);
Coord3D operator-(const Coord3D &a, const Coord3D &b);

#endif